/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabajofinal;

import javax.swing.JOptionPane;

/**
 *
 * @author niko
 */
public class Menu {

    //MENU GRAL
    public static void menu() {

        int menu = JOptionPane.showOptionDialog(null,
                "Es necesario que seleccione una opcion",
                "FINAL Requerimintos de software",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                new Object[]{"Calculadora", "Perimetro", "Area"},
                "opcion 1");

        if (menu != -1) {
            switch (menu) {
                case 0:
                    menuC();
                    break;
                case 1:
                    menuP();
                    break;
                default:
                    menuA();
                    break;
            }
        } else {
            menuX();
        }

    }

    //------------------------------------------
    //MENU CALC
    public static void menuC() {
        int menuC = JOptionPane.showOptionDialog(null,
                "Es necesario que seleccione una opcion",
                "FINAL Requerimintos de software",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                new Object[]{"Suma", "Resta", "Multiplicación", "Divición"},
                "opcion 1");
        if (menuC != -1) {
            switch (menuC) {
                case 0:
                    CalculadoraFinal.cargarValores();
                    CalculadoraFinal.mostrarS();
                    break;
                case 1:
                    CalculadoraFinal.cargarValores();
                    CalculadoraFinal.mostrarR();
                    break;
                case 2:
                    CalculadoraFinal.cargarValores();
                    CalculadoraFinal.mostrarM();
                    break;
                default:
                    CalculadoraFinal.cargarValores();
                    CalculadoraFinal.mostrarD();
                    break;
            }
        } else {
            menuX();
        }

    }

    //------------------------------------------
    //MENU PERIMETRO
    public static void menuP() {
        int menuP = JOptionPane.showOptionDialog(null,
                "Es necesario que seleccione una opcion",
                "FINAL Requerimintos de software",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                new Object[]{"Triangulo equilátero", "Cuadrado"},
                "opcion 1");
        if (menuP != -1) {
            switch (menuP) {
                case 0:
                    PerimFinal.triangulo();
                    PerimFinal.mostrarT();
                    break;
                case 1:
                    PerimFinal.caudrado();
                    PerimFinal.mostrarC();
                    break;
                default:
                    menuX();
                    break;
            }

        }

    }

    //------------------------------------------
    //MENU AREA
    public static void menuA() {
        int menuA = JOptionPane.showOptionDialog(null,
                "Es necesario que seleccione una opcion",
                "FINAL Requerimintos de software",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                new Object[]{"Triangulo equilátero", "Cuadrado"},
                "opcion 1");
        if (menuA != -1) {
            switch (menuA) {
                case 0:
                    AreaFinal.triangulo();
                    AreaFinal.mostrarT();
                    break;
                case 1:
                    AreaFinal.caudrado();
                    AreaFinal.mostrarC();
                    break;
                default:
                    menuX();
                    break;
            }

        }

    }

    //------------------------------------------
    //MENU X
    public static void menuX() {
        JOptionPane.showMessageDialog(
                null,
                "Muchas gracias por usar el programa");

    }

}
