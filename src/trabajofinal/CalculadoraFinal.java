/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabajofinal;

import javax.swing.JOptionPane;

/**
 *
 * @author niko
 */
public class CalculadoraFinal {
    
    
    public static void cargarValores(){
        String valorUno = JOptionPane.showInputDialog("Ingresar un primer valor: ");
        valor1 = Float.parseFloat(valorUno);
        
        String valorDos = JOptionPane.showInputDialog("Ingresar un segundo valor: ");
        valor2 = Float.parseFloat(valorDos);
      
    }
    
    private static float valor1;
    private static float valor2;
    
    private static float suma(){
        float resulSuma = valor1 + valor2;
        return resulSuma;
        
    }
    private static float resta(){
        float resulResta = valor1 - valor2;
        return resulResta;
        
    }
    private static float div(){
        float resulDiv = valor1 / valor2;
        return resulDiv;
        
    }
    private static float mult(){
        float resulMult = valor1 * valor2;
        return resulMult;
        
    }

    
    public static void mostrarS(){
        JOptionPane.showMessageDialog(null, "El resultado es: "+ suma());
            
    }
    public static void mostrarR(){
        JOptionPane.showMessageDialog(null, "El resultado es: "+ resta());
            
    }
    public static void mostrarD(){
        JOptionPane.showMessageDialog(null, "El resultado es: "+ div());
            
    }
    public static void mostrarM(){
        JOptionPane.showMessageDialog(null, "El resultado es: "+ mult());
            
    }
}   
    

