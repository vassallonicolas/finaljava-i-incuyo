/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabajofinal;

import javax.swing.JOptionPane;

/**
 *
 * @author niko
 */
public class AreaFinal {
    
    //AREA DE TRIANGULO EQUILATERO
    public static void triangulo() {
        String valorBase = JOptionPane.showInputDialog("Ingresar el valor de la base: ");
        String valorAltura = JOptionPane.showInputDialog("Ingresar el valor de la altura: ");
        valorB = Float.parseFloat(valorBase);
        valorA = Float.parseFloat(valorAltura);

    }
    
    private static float valorB;
    private static float valorA;

    //FORMULA TRIANGULO EQUILATERO
    private static float resultadoT() {
        float resul = (valorB * valorA) / 2;
        return resul;

    }
    //RESULTADO

    public static void mostrarT() {
        JOptionPane.showMessageDialog(null, "El area del triangulo Equilatero es : " + resultadoT());

    }
    //------------------------------------------

    //AREA DEL CUADRADO
    public static void caudrado() {
        String valorLado = JOptionPane.showInputDialog("Ingresar del lado: ");
        valorL = Float.parseFloat(valorLado);

    }
    private static float valorL;

    //FORMULA DEL CUADRADO
    private static float resultadoC() {
        float resulC = valorL * valorL;
        return resulC;

    }

    //RESULTADO
    public static void mostrarC() {
        JOptionPane.showMessageDialog(null, "El area del Cuadrado es : " + resultadoC());

    }

}
